from flask import Flask
import os

app = Flask(__name__)


@app.route('/health')
def get_health():
    return {'status': 'OK'}


@app.route("/")
def hello():
    current_env = os.environ
    if 'HOSTNAME' in current_env:
        return 'HOSTNAME is ' + current_env['HOSTNAME'] + '!'
    else:
        return "Can't get HOSTNAME"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
