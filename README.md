# OTUS Architect homework

## Директории

- `/app` - директория с простым приложением на Python
- `/manifests` - директория с манифестами k8s

## Запуск

Команда `kubectl apply -f manifests/` развернет приложение в кластере. Ингресс настроен на хост `arch.homework`.

Для проверки работоспособности необходимо выполнить:
- `curl -H "HOST: arch.homework" $(minikube ip)` для получения имены ноды
- `curl -H "HOST: arch.homework" $(minikube ip)/health` для получения статуса приложения (health check)
- `curl -H "HOST: arch.homework" $(minikube ip)/otusapp/njxqlus/` для проверки редиректа на корень приложения

Для упрощения запросов можно прописать хост `arch.homework` в хосты (/etc/hosts) машины.
